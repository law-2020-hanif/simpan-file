from django.urls import path

from . import views

urlpatterns = [
    path('simpan', views.controller, name='controller'),
    path('download/<str:file_id>/', views.downloadFile, name='downloadFile'),
    path('stored', views.list_of_downloadable_files, name='list_of_downloadable_files')
]
