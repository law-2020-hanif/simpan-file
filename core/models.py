from django.db import models
import os, socket
# Create your models here.

class Dokumen(models.Model):
    dokumen = models.FileField(upload_to='uploads/')
    oauth_client_id = models.CharField(max_length=100)

    def filename(self):
        return os.path.basename(self.dokumen.name)
