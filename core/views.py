from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, FileResponse, JsonResponse
import os, requests, json, socket
from .models import Dokumen
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
PATH_TO_UPLOADED_FILES_FOLDER = os.path.join(settings.BASE_DIR, 'uploaded-files', 'uploads/')

@csrf_exempt
def controller(request):
    if request.method == 'POST':
        params = areParametersCompleted(request.POST)
        if (len(params) != 0):
            message = generateIncompleteParametersMessage(params)
            return JsonResponse(jsonErrorMessage(message))
        else:
            generated_token = ambil_token(request.POST['username'], request.POST['password'], request.POST['client_id'], request.POST['client_secret'])

            if 'error' in generated_token:
                return JsonResponse(generated_token, safe=False)
            acquired_resources = ambil_resource(generated_token['access_token'])

            if 'error' in acquired_resources:
                return JsonResponse(acquired_resources, safe=False)
            return saveFileAsClientId(acquired_resources['client_id'], request.FILES, request.META, generated_token['access_token'])
    return JsonResponse(jsonErrorMessage('untuk melakukan kompresi file, gunakan method POST'))

def ambil_token(user, pasw, client_id, client_secret):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/token"
    payload = {'username': user, 'password': pasw, 'grant_type': 'password', 'client_id': client_id, 'client_secret': client_secret}
    r = requests.post(url = URL, data=payload)
    return json.loads(r.text)

def ambil_resource(access_token):
    URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.get(url = URL, headers = data_headers)
    return json.loads(r.text)

def areParametersCompleted(request_body):
    incomplete_params = []
    required_fields = ['username', 'password', 'client_id', 'client_secret']
    for required_field in required_fields:
        if required_field not in request_body:
            incomplete_params.append(required_field)
    return incomplete_params

def generateIncompleteParametersMessage(list_of_incomplete_params):
    string_response = 'parameter yang dibutuhkan untuk OAUTH tidak lengkap. Parameter yang kurang : '
    for incomplete_param in list_of_incomplete_params:
        string_response = string_response + incomplete_param + ', '
    return string_response[:-2]

def jsonErrorMessage(msg):
    return {'error': msg}

def saveFileAsClientId(client_id, request_file, meta, access_token):
    document_object = Dokumen.objects.create(dokumen=request_file['file'], oauth_client_id = client_id)

    file_size = os.path.getsize(PATH_TO_UPLOADED_FILES_FOLDER + document_object.filename())
    hitMetadataService(client_id, file_size, 'http://152.118.148.95:' + meta['SERVER_PORT'] + '/download/' + str(document_object.id), document_object.filename(), access_token)

    return JsonResponse({
        'nama_file':document_object.filename(),
        'download_url': 'http://' + meta['SERVER_NAME'] + ':' + meta['SERVER_PORT'] + '/download/' + str(document_object.id),
        'download_url_from_infralabs': 'http://152.118.148.95:' + meta['SERVER_PORT'] + '/download/' + str(document_object.id)
    })

def downloadFile(request, file_id):
    if isAuthorizationHeaderOkay(request.headers) is False:
        return JsonResponse(jsonErrorMessage('silakan isi Authorization pada header dengan value: Bearer [token auth]'))
    try:
        document_object = Dokumen.objects.get(id=file_id)
        if isAuthorizedToDownloadFile(request.headers['Authorization'].split()[1], document_object.oauth_client_id) is False:
            return JsonResponse(jsonErrorMessage('Anda tidak memiliki otorisasi untuk mengakses file tersebut'))
        desired_file = open(PATH_TO_UPLOADED_FILES_FOLDER + document_object.filename(), 'rb')
        return FileResponse(desired_file)


    except ObjectDoesNotExist:
        return JsonResponse(jsonErrorMessage('file tidak ditemukan'))

def isAuthorizationHeaderOkay(request_headers):
    if ('Authorization' not in request_headers) or (request_headers['Authorization'] is None) or (request_headers['Authorization'].split()[1] is None):
        return False
    return True

def isAuthorizedToDownloadFile(access_token, oauth_client_id_file):
    acquired_resources = ambil_resource(access_token)
    if 'error' in acquired_resources:
        return False
    if acquired_resources['client_id'] == oauth_client_id_file:
        return True
    return False

def list_of_downloadable_files(request):
    if isAuthorizationHeaderOkay(request.headers) is False:
        return JsonResponse(jsonErrorMessage('silakan isi Authorization pada header dengan value: Bearer [token auth]'))
    
    acquired_resources = ambil_resource(request.headers['Authorization'].split()[1])
    if 'error' in acquired_resources:
        return JsonResponse(acquired_resources, safe=False)
    list_of_users_files = Dokumen.objects.all().filter(oauth_client_id = acquired_resources['client_id'])

    list_of_ouput = []
    for user_file in list_of_users_files:
        list_of_ouput.append(
            {'nama_file':user_file.filename(), 
            'download_url': 'http://' + request.META['SERVER_NAME'] + ':' + request.META['SERVER_PORT'] + '/download/' + str(user_file.id),
            'download_url_from_infralabs': 'http://152.118.148.95:' + request.META['SERVER_PORT'] + '/download/' + str(user_file.id)}
        )

    return JsonResponse(list_of_ouput, safe=False)
    
def hitMetadataService(oauth_client_id, file_size, downloadable_url, file_name, access_token):
    metadata_URL = 'http://152.118.148.95:20355/metadata/new'
    payload = {'oauth_client_id': oauth_client_id, 'file_size': file_size, 'downloadable_url': downloadable_url, 'file_name': file_name}
    data_headers = { 'Authorization': 'Bearer ' + access_token }
    r = requests.post(url = metadata_URL, data=payload, headers = data_headers)
    return json.loads(r.text)